No docs yet.

When measuring absorption coefficients in sea water with e.g. a WetLabs AC-9 or Hobilabs A-sphere, the measured values have to be corrected for the effects of temperature and salinity.
This takes `pandas` `DataFrame`s of absorption coefficients and CTD-data and applies such corrections.

There is a choice of four different sources for the coefficients:


 - Langford, V.S., McKinley, A.J., Quickenden, T.I., 2001. Temperature Dependence of the Visible-Near-Infrared Absorption Spectrum of Liquid Water. The Journal of Physical Chemistry A 105, 8916–8921. doi:10.1021/jp010093m
 - Pegau, W.S., Gray, D., Zaneveld, J.R.V., 1997. Absorption and attenuation of visible and near-infrared light in water: dependence on temperature and salinity. Applied Optics 36, 6035. doi:10.1364/AO.36.006035
 - Röttgers, R., McKee, D., Utschig, C., 2014. Temperature and salinity correction coefficients for light absorption by water in the visible to infrared spectral region. Optics Express 22, 25093. doi:10.1364/OE.22.025093
 - Sullivan, J.M., Twardowski, M.S., Zaneveld, J.R.V., Moore, C.M., Barnard, A.H., Donaghay, P.L., Rhoades, B., 2006. Hyperspectral temperature and salt dependencies of absorption by water and heavy water in the 400-750 nm spectral range. Applied Optics 45, 5294. doi:10.1364/AO.45.005294
