from distutils.core import setup

setup(name='abscorr',
      version='0.1',
      description='Temperature and salinity correction of absorption',
      author='Torbjørn Taskjelle',
      author_email='Torbjorn.Taskjelle@ift.uib.no',
      packages=['abscorr'],
      package_dir={'abscorr': 'src/abscorr'},
      package_data={'abscorr': ['data/*.csv'] }
      )
