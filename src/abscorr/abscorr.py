import pandas as pd
import numpy as np
import os
import abscorr
import warnings

def getcoeffs(author='pegau'):
    '''Valid choices for author are 'pegau', 'langford','sullivan' and 'rottger'.'''
    author = author.lower()[:5]
    basefolder = abscorr.__path__[0]
    if author=='pegau':
        fn = os.path.join(basefolder,'data/pegau1997.csv')
    elif author=='pegam':
        fn = os.path.join(basefolder,'data/pegau1997_m.csv')
    elif author=='langf':
        fn = os.path.join(basefolder,'data/langford2001.csv')
    elif author=='sulli':
        fn = os.path.join(basefolder,'data/sullivan2006.csv')
    elif author=='rottg':
        fn = os.path.join(basefolder,'data/rottger2014.csv')

    return pd.read_csv(fn,index_col=0)

    
def interpcoeffs(wlin,psiT,var='psiT',highalt='langford',lowalt='rottger'):
    if var[3] == 'S':
        hvar = 'psiSASW'
        lvar = 'psiSASW'
    else:
        hvar,lvar = var, var
    wlin = np.array(wlin)
    if wlin.min() >= psiT.index.min() and wlin.max() <= psiT.index.max():
        interpcoeff = np.interp(wlin,psiT.index,psiT[var])
        
    elif wlin.min() < psiT.index.min() and wlin.max() <= psiT.index.max():
        print('Lower wavelengths out of range, using values from {0} in this range'.format(lowalt))
        lcoef = getcoeffs(lowalt)
        interpcoeff = np.interp(wlin,psiT.index,psiT[var],left=np.nan)
        nanind = np.isnan(interpcoeff)
        interpcoeff[nanind] = np.interp(wlin[nanind],lcoef.index,lcoef[lvar])
        
        
    elif wlin.max() > psiT.index.max() and wlin.min() >= psiT.index.min():
        print('Higher wavelengths out of range, using values from {0} in this range'.format(highalt))
        interpcoeff = np.interp(wlin,psiT.index,psiT[var],right=np.nan)
        hcoef = getcoeffs(highalt)
        nanind = np.isnan(interpcoeff)
        interpcoeff[nanind] = np.interp(wlin[nanind],hcoef.index,hcoef[hvar])

    elif wlin.max() > psiT.index.max() and wlin.min() < psiT.index.min():
        print('Higher and lower wavelengths out of range, using values from {0} and {1}'.format(highalt,lowalt))
        interpcoeff = np.interp(wlin,psiT.index,psiT[var],right=np.nan,left=-10000)
        lcoef = getcoeffs(lowalt)
        hcoef = getcoeffs(highalt)
        nanind = np.isnan(interpcoeff)
        lind = (interpcoeff == -10000)
        interpcoeff[nanind] = np.interp(wlin[nanind],hcoef.index,hcoef[hvar])
        interpcoeff[lind] = np.interp(wlin[lind],lcoef.index,lcoef[lvar])

    return interpcoeff

        
def tempcorrect(a,t,reftemp,source='pegau'):
    if not all(a.index.isin(t.index)):
         warnings.warn('Not all index values in absorption-DataFrame are found in temperature-DataFrame')

    if not isinstance(t,pd.Series):
        try:
            t = t.temp
        except:
            raise NameError("Couldn't find column named temp in temperature dataframe")
        
    wl = np.array(a.columns,dtype='float64')

    newa = pd.DataFrame(index=a.index,columns=a.columns)

    psiT = getcoeffs(source)
    psiT = interpcoeffs(wl,psiT)

    for psi,wi in zip(psiT,a.columns):
        newa.loc[:,wi] = a.loc[:,wi] + (reftemp - t) * psi

    return newa


def saltcorrect(a,S,source='sullivan'):
    if not all(a.index.isin(S.index)):
         warnings.warn('Not all index values in absorption-DataFrame are found in salinity-DataFrame')

    if not isinstance(S,pd.Series):
        try:
            S = S.sal
        except:
            raise NameError("Couldn't find column named temp in temperature dataframe")
        
    wl = np.array(a.columns,dtype='float64')

    newa = pd.DataFrame(index=a.index,columns=a.columns)

    psiS = getcoeffs(source)
    if source == 'rottgerN':
        psiS = psiS.ix[400:2658]
        var = 'psiSNaCl'
    elif source == 'rottgerA':
        psiS = psiS.ix[400:]
        var = 'psiSASW'
    else:
        var='psiS'
        
    psiS = interpcoeffs(wl,psiS,var=var,highalt='rottgerA',lowalt='rottgerA')

    for psi,wi in zip(psiS,a.columns):
        newa[wi] = a[wi] - S * psi

    return newa
    
        


        
